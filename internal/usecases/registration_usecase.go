// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package usecases

import (
	"encoding/json"
	"errors"
	"fmt"
	"time"

	"github.com/golang-jwt/jwt/v5"
	"github.com/google/uuid"
	"gitlab.com/blauwe-knop/common/health-checker/pkg/healthcheck"
	"gitlab.com/blauwe-knop/connect/go-connect/crypto/aes"
	"gitlab.com/blauwe-knop/connect/go-connect/crypto/ec"
	"gitlab.com/blauwe-knop/connect/go-connect/crypto/ecdsa"
	"gitlab.com/blauwe-knop/connect/go-connect/encoding/base64"
	"go.uber.org/zap"

	serviceModel "gitlab.com/blauwe-knop/vorderingenoverzicht/app-management-service/pkg/model"
	serviceRepositories "gitlab.com/blauwe-knop/vorderingenoverzicht/app-management-service/pkg/repositories"
	bkConfigModel "gitlab.com/blauwe-knop/vorderingenoverzicht/bk-config-service/pkg/model"
	bkConfigRepositories "gitlab.com/blauwe-knop/vorderingenoverzicht/bk-config-service/pkg/repositories"
	sessionRepositories "gitlab.com/blauwe-knop/vorderingenoverzicht/session-service/pkg/repositories"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/app-management-process/pkg/events"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/app-management-process/pkg/model"
)

type RegistrationUseCase struct {
	Logger                       *zap.Logger
	RegistrationRepository       serviceRepositories.RegistrationRepository
	SessionRepository            sessionRepositories.SessionRepository
	BkConfigRepository           bkConfigRepositories.BkConfigRepository
	RegistrationExpirationHours  int
	CertificateExpirationMinutes int
}

const leewayNotBeforeMinutes int = -2

var ErrKeyPairDoesNotExist = errors.New("error key pair does not exist")
var ErrSessionExpired = errors.New("error session expired")
var ErrRegistrationExpired = errors.New("error registration expired")

func NewRegistrationUseCase(logger *zap.Logger, registrationRepository serviceRepositories.RegistrationRepository, sessionRepository sessionRepositories.SessionRepository, bkConfigRepository bkConfigRepositories.BkConfigRepository, registrationExpirationHours int, certificateExpirationMinutes int) *RegistrationUseCase {
	return &RegistrationUseCase{
		Logger:                       logger,
		RegistrationRepository:       registrationRepository,
		SessionRepository:            sessionRepository,
		BkConfigRepository:           bkConfigRepository,
		RegistrationExpirationHours:  registrationExpirationHours,
		CertificateExpirationMinutes: certificateExpirationMinutes,
	}
}

func (uc *RegistrationUseCase) RegisterApp(sessionToken string, encryptedAppIdentity base64.Base64String) (base64.Base64String, error) {
	session, err := uc.SessionRepository.GetSession(sessionToken)
	if err != nil {
		event := events.AMP_27
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return "", fmt.Errorf("failed to get session: %v", err)
	}

	if time.Now().After((time.Time)(session.ExpiresAt)) {
		event := events.AMP_39
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return "", ErrSessionExpired
	}

	sessionAesKey, err := base64.Base64String(session.AesKey).ToBytes()
	if err != nil {
		event := events.AMP_56
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return "", fmt.Errorf("failed to decode session aes key: %v", err)
	}

	if !aes.VerifyKey(sessionAesKey) {
		event := events.AMP_57
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
		return "", fmt.Errorf("session aes key invalid")
	}

	ciphertext, err := encryptedAppIdentity.ToBytes()
	if err != nil {
		event := events.AMP_30
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return "", fmt.Errorf("failed to decode ciphertext from request: %v", err)
	}

	decryptAppIdentity, err := aes.Decrypt(sessionAesKey, ciphertext)
	if err != nil {
		event := events.AMP_31
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return "", fmt.Errorf("failed to decrypt app identity: %v", err)
	}

	var appIdentity model.AppIdentity
	err = json.Unmarshal([]byte(decryptAppIdentity), &appIdentity)
	if err != nil {
		event := events.AMP_32
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return "", fmt.Errorf("failed to unmarshal appIdentity: %v", err)
	}

	if session.AppPublicKey != appIdentity.AppPublicKey {
		event := events.AMP_33
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
		return "", fmt.Errorf("session app public key and app identity public does not match")
	}

	ecAppPublicKey, err := ec.ParsePublicKeyFromPem([]byte(appIdentity.AppPublicKey))
	if err != nil {
		event := events.AMP_34
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
		return "", fmt.Errorf("failed to parse app public key to pem")
	}

	if !ecAppPublicKey.Verify() {
		event := events.AMP_54
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
		return "", fmt.Errorf("app public key invalid")
	}

	expiresAt := time.Now().Add(time.Minute * 60 * time.Duration(uc.RegistrationExpirationHours))
	registration := serviceModel.Registration{
		Token:        uuid.NewString(),
		AppPublicKey: appIdentity.AppPublicKey,
		CreatedAt:    serviceModel.JSONTime(time.Now()),
		ExpiresAt:    serviceModel.JSONTime(expiresAt),
		ClientId:     appIdentity.ClientId,
	}

	createdRegistration, err := uc.RegistrationRepository.Create(registration)
	if err != nil {
		event := events.AMP_35
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return "", fmt.Errorf("failed to create registration: %v", err)
	}

	encryptedRegistrationToken, err := aes.Encrypt(sessionAesKey, []byte(createdRegistration.Token))
	if err != nil {
		event := events.AMP_58
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return "", fmt.Errorf("failed to encrypt registration token: %v", err)
	}

	return base64.ParseFromBytes(encryptedRegistrationToken), nil
}

func (uc *RegistrationUseCase) LinkUserIdentity(registrationToken string, userIdentity model.UserIdentity) (*serviceModel.Registration, error) {
	registration, err := uc.RegistrationRepository.Get(registrationToken)
	if err != nil {
		event := events.AMP_36
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return nil, fmt.Errorf("failed to get registration session: %v", err)
	}

	if time.Now().After((time.Time)(registration.ExpiresAt)) {
		event := events.AMP_37
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return nil, ErrRegistrationExpired
	}

	registration.Bsn = userIdentity.Bsn

	updatedRegistration, err := uc.RegistrationRepository.Update(registration.Token, *registration)
	if err != nil {
		event := events.AMP_38
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return nil, fmt.Errorf("failed to update registration session: %v", err)
	}

	return updatedRegistration, nil
}

func (uc *RegistrationUseCase) FetchCertificate(sessionToken string, registrationToken string) (base64.Base64String, error) {
	session, err := uc.SessionRepository.GetSession(sessionToken)
	if err != nil {
		event := events.AMP_27
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return "", fmt.Errorf("failed to get session: %v", err)
	}

	if time.Now().After((time.Time)(session.ExpiresAt)) {
		event := events.AMP_39
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return "", ErrSessionExpired
	}

	registration, err := uc.RegistrationRepository.Get(registrationToken)
	if err != nil {
		event := events.AMP_40
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return "", fmt.Errorf("failed to get registration: %v", err)
	}

	if time.Now().After((time.Time)(registration.ExpiresAt)) {
		event := events.AMP_37
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return "", ErrRegistrationExpired
	}

	if session.AppPublicKey != registration.AppPublicKey {
		event := events.AMP_33
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
		return "", fmt.Errorf("session app public key and app identity public does not match")
	}

	ecAppPublicKey, err := ec.ParsePublicKeyFromPem([]byte(registration.AppPublicKey))
	if err != nil {
		event := events.AMP_34
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return "", fmt.Errorf("failed to decode app public key from pem: %v", err)
	}

	if !ecAppPublicKey.Verify() {
		event := events.AMP_54
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
		return "", fmt.Errorf("app public key invalid")
	}

	organizationRootPrivateKey, err := uc.getOrganizationRootPrivateKey()
	if err != nil {
		event := events.AMP_41
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return "", fmt.Errorf("failed retrieve key pair: %v", err)
	}

	ecOrganizationRootPrivateKey, err := ec.ParsePrivateKeyFromPem([]byte(organizationRootPrivateKey.PrivateKey))
	if err != nil {
		event := events.AMP_29
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return "", fmt.Errorf("failed to decode private key from pem: %v", err)
	}

	now := time.Now().UTC()

	var givenName string
	var familyName string

	// begin demo data
	switch registration.Bsn {
	case "814859094":
		givenName = "Piet"
		familyName = "Verstraten"
	case "309777938":
		givenName = "Barbara"
		familyName = "Goulouse"
	case "950053545":
		givenName = "Çigdem"
		familyName = "Kemal"
	case "761582915":
		givenName = "Hendrik-jan"
		familyName = "Jansens"
	case "570957588":
		givenName = "Lance"
		familyName = "Smit"
	case "999992740":
		givenName = "Jael"
		familyName = "de Jager"
	default:
		givenName = "Henk"
		familyName = "Smit"
	}
	// end demo data

	claims := make(jwt.MapClaims)
	claims["app_public_key"] = registration.AppPublicKey
	claims["app_manager_oin"] = "00000001001172773000"
	claims["app_manager_public_key"] = organizationRootPrivateKey.PublicKey
	claims["given_name"] = givenName
	claims["family_name"] = familyName
	claims["bsn"] = registration.Bsn
	claims["scope"] = "nl.vorijk.oauth_scope.blauwe_knop"
	claims["iat"] = now.Unix()
	claims["nbf"] = now.Add(time.Minute * time.Duration(leewayNotBeforeMinutes)).Unix()
	claims["exp"] = now.Add(time.Minute * time.Duration(uc.CertificateExpirationMinutes)).Unix()

	accessToken, err := jwt.NewWithClaims(jwt.SigningMethodES256, claims).SignedString(ecOrganizationRootPrivateKey.ToEcdsa())
	if err != nil {
		event := events.AMP_43
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return "", fmt.Errorf("failed to sign access token: %w", err)
	}

	certificate := model.Certificate{
		Type:  "AppManagerJWTCertificate",
		Value: []byte(accessToken),
	}

	certificateAsJson, err := json.Marshal(certificate)
	if err != nil {
		event := events.AMP_44
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return "", fmt.Errorf("failed to marshal certificate: %v", err)
	}

	sessionAesKey, err := base64.Base64String(session.AesKey).ToBytes()
	if err != nil {
		event := events.AMP_56
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return "", fmt.Errorf("failed to decode session aes key: %v", err)
	}

	if !aes.VerifyKey(sessionAesKey) {
		event := events.AMP_57
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
		return "", fmt.Errorf("session aes key invalid")
	}

	encryptedCertificate, err := aes.Encrypt(sessionAesKey, certificateAsJson)
	if err != nil {
		event := events.AMP_45
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return "", fmt.Errorf("failed to encrypt certificate: %v", err)
	}

	encodedCertificate := base64.ParseFromBytes(encryptedCertificate)
	if encodedCertificate == "" {
		event := events.AMP_46
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return "", fmt.Errorf("failed to encode encrypted certificate: %v", err)
	}

	return encodedCertificate, nil
}

func (uc *RegistrationUseCase) UnregisterApp(sessionToken string, unregisterAppRequest model.UnregisterAppRequest) error {
	session, err := uc.SessionRepository.GetSession(sessionToken)
	if err != nil {
		event := events.AMP_27
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return fmt.Errorf("failed to get session: %v", err)
	}

	if time.Now().After((time.Time)(session.ExpiresAt)) {
		event := events.AMP_39
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return ErrSessionExpired
	}

	appPublicKey, err := ec.ParsePublicKeyFromPem([]byte(session.AppPublicKey))
	if err != nil {
		event := events.AMP_34
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return fmt.Errorf("failed to decode app public key from pem: %v", err)
	}

	signature, err := base64.Base64String(unregisterAppRequest.Signature).ToBytes()
	if err != nil {
		event := events.AMP_55
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return fmt.Errorf("failed to decode signature: %v", err)
	}

	verified := ecdsa.Verify(appPublicKey, []byte(unregisterAppRequest.RegistrationToken), signature)
	if !verified {
		event := events.AMP_48
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return errors.New("invalid registration signature")
	}

	_, err = uc.RegistrationRepository.Get(unregisterAppRequest.RegistrationToken)
	if err != nil {
		event := events.AMP_40
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return fmt.Errorf("failed to get registration: %v", err)
	}

	_, err = uc.RegistrationRepository.Delete(unregisterAppRequest.RegistrationToken)
	if err != nil {
		event := events.AMP_49
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return fmt.Errorf("failed to delete registration: %v", err)
	}

	return nil
}

func (uc *RegistrationUseCase) getOrganizationRootPrivateKey() (*bkConfigModel.KeyPair, error) {
	// current implementation: we always pick the first one if multiple keys exist
	keyPairList, err := uc.BkConfigRepository.ListKeyPairs()
	if err != nil {
		event := events.AMP_50
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return nil, fmt.Errorf("failed to list key pairs: %v", err)
	}

	if len(*keyPairList) == 0 {
		event := events.AMP_51
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return nil, ErrKeyPairDoesNotExist
	}

	keyPair := (*keyPairList)[0]

	return &keyPair, nil
}

func (uc *RegistrationUseCase) GetHealthChecks() []healthcheck.Checker {
	return []healthcheck.Checker{
		uc.RegistrationRepository,
		uc.RegistrationRepository,
	}
}
