package usecases_test

import (
	"crypto/elliptic"
	"encoding/json"
	"errors"
	"fmt"
	"testing"
	"time"

	"github.com/golang-jwt/jwt/v5"
	"github.com/stretchr/testify/assert"
	"go.uber.org/mock/gomock"
	"go.uber.org/zap"
	"go.uber.org/zap/zaptest/observer"

	"gitlab.com/blauwe-knop/connect/go-connect/crypto/aes"
	"gitlab.com/blauwe-knop/connect/go-connect/crypto/ec"
	"gitlab.com/blauwe-knop/connect/go-connect/encoding/base64"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/app-management-process/internal/usecases"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/app-management-process/pkg/model"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/app-management-process/test/mock"

	serviceModel "gitlab.com/blauwe-knop/vorderingenoverzicht/app-management-service/pkg/model"
	serviceRepositories "gitlab.com/blauwe-knop/vorderingenoverzicht/app-management-service/pkg/repositories"
	bkConfigServiceModel "gitlab.com/blauwe-knop/vorderingenoverzicht/bk-config-service/pkg/model"
	bkConfigRepositories "gitlab.com/blauwe-knop/vorderingenoverzicht/bk-config-service/pkg/repositories"
	sessionServiceModel "gitlab.com/blauwe-knop/vorderingenoverzicht/session-service/pkg/model"
	sessionRepositories "gitlab.com/blauwe-knop/vorderingenoverzicht/session-service/pkg/repositories"
)

func TestRegistrationUseCase_RegisterApp(t *testing.T) {
	observedZapCore, _ := observer.New(zap.InfoLevel)

	observedLogger := zap.New(observedZapCore)

	appKeyPair, err := ec.GenerateKeyPair(elliptic.P256())
	assert.NoError(t, err)

	appPublicKeyPem, err := appKeyPair.PublicKey.ToPem()
	assert.NoError(t, err)

	sessionAesKey, err := aes.GenerateKey(aes.AES128)
	assert.NoError(t, err)

	type fields struct {
		RegistrationRepository       serviceRepositories.RegistrationRepository
		SessionRepository            sessionRepositories.SessionRepository
		BkConfigRepository           bkConfigRepositories.BkConfigRepository
		RegistrationExpirationDays   int
		CertificateExpirationMinutes int
	}
	type args struct {
		sessionToken         string
		encryptedAppIdentity base64.Base64String
	}

	tests := []struct {
		name                      string
		fields                    fields
		args                      args
		expectedError             error
		expectedRegistrationToken string
	}{
		{
			name: "returns a registration token",
			fields: fields{
				RegistrationRepository: func() serviceRepositories.RegistrationRepository {
					controller := gomock.NewController(t)

					mockRegistrationRepository := mock.NewMockRegistrationRepository(controller)

					mockRegistrationRepository.
						EXPECT().
						Create(gomock.Any()).
						Return(&serviceModel.Registration{
							Token: "ValidRegistrationToken",
						}, nil)

					return mockRegistrationRepository
				}(),
				SessionRepository: func() sessionRepositories.SessionRepository {
					controller := gomock.NewController(t)

					mockSessionRepository := mock.NewMockSessionRepository(controller)

					dateTime := time.Now().Add(time.Hour * 24)

					mockSessionRepository.
						EXPECT().
						GetSession("validSessionToken").
						Return(&sessionServiceModel.Session{
							Token:        "validSessionToken",
							AppPublicKey: string(appPublicKeyPem),
							AesKey:       base64.ParseFromBytes(sessionAesKey),
							Scope:        "",
							Bsn:          "",
							CreatedAt:    sessionServiceModel.JSONTime{},
							ExpiresAt:    sessionServiceModel.JSONTime(dateTime),
						}, nil)

					return mockSessionRepository
				}(),
				BkConfigRepository: nil,
			},
			args: args{
				sessionToken: "validSessionToken",
				encryptedAppIdentity: func() base64.Base64String {

					appIdentity := &model.AppIdentity{
						AppPublicKey: string(appPublicKeyPem),
						ClientId:     "",
					}

					appIdentityAsJson, err := json.Marshal(appIdentity)
					assert.NoError(t, err)

					encryptedIdentity, err := aes.Encrypt(sessionAesKey, appIdentityAsJson)
					assert.NoError(t, err)

					return base64.ParseFromBytes(encryptedIdentity)
				}(),
			},
			expectedError:             nil,
			expectedRegistrationToken: "ValidRegistrationToken",
		}, {
			name: "returns error session app public key and app identity public does not match",
			fields: fields{
				RegistrationRepository: nil,
				SessionRepository: func() sessionRepositories.SessionRepository {
					controller := gomock.NewController(t)

					mockSessionRepository := mock.NewMockSessionRepository(controller)

					dateTime := time.Now().Add(time.Hour * 24)

					appPublicKeyPem, err := appKeyPair.PublicKey.ToPem()
					assert.NoError(t, err)

					mockSessionRepository.
						EXPECT().
						GetSession("validSessionToken").
						Return(&sessionServiceModel.Session{
							Token:        "validSessionToken",
							AppPublicKey: string(appPublicKeyPem),
							AesKey:       base64.ParseFromBytes(sessionAesKey),
							Scope:        "",
							Bsn:          "",
							CreatedAt:    sessionServiceModel.JSONTime(dateTime),
							ExpiresAt:    sessionServiceModel.JSONTime(dateTime),
						}, nil)

					return mockSessionRepository
				}(),
				BkConfigRepository: nil,
			},
			args: args{
				sessionToken: "validSessionToken",
				encryptedAppIdentity: func() base64.Base64String {

					appIdentity := &model.AppIdentity{
						AppPublicKey: "",
						ClientId:     "",
					}

					appIdentityAsJson, err := json.Marshal(appIdentity)
					assert.NoError(t, err)

					encryptedIdentity, err := aes.Encrypt(sessionAesKey, appIdentityAsJson)
					assert.NoError(t, err)

					return base64.ParseFromBytes(encryptedIdentity)
				}(),
			},
			expectedError: fmt.Errorf("session app public key and app identity public does not match"),
		},
		{
			name: "returns error as session does not exists",
			fields: fields{
				RegistrationRepository: nil,
				SessionRepository: func() sessionRepositories.SessionRepository {
					controller := gomock.NewController(t)

					mockSessionRepository := mock.NewMockSessionRepository(controller)

					mockSessionRepository.
						EXPECT().
						GetSession("invalidSessionToken").
						Return(nil, errors.New("session not found"))

					return mockSessionRepository
				}(),
			},
			args: args{
				sessionToken:         "invalidSessionToken",
				encryptedAppIdentity: "",
			},
			expectedError: fmt.Errorf("failed to get session: %v", errors.New("session not found")),
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			uc := usecases.NewRegistrationUseCase(
				observedLogger,
				tt.fields.RegistrationRepository,
				tt.fields.SessionRepository,
				tt.fields.BkConfigRepository,
				tt.fields.RegistrationExpirationDays,
				tt.fields.CertificateExpirationMinutes,
			)

			actualRegistrationToken, err := uc.RegisterApp(tt.args.sessionToken, tt.args.encryptedAppIdentity)
			assert.Equal(t, tt.expectedError, err)

			var decryptedRegistrationToken string
			if err == nil {
				actualRegistrationTokenBytes, err := actualRegistrationToken.ToBytes()
				assert.NoError(t, err)

				decryptedRegistrationTokenBytes, err := aes.Decrypt(sessionAesKey, actualRegistrationTokenBytes)
				assert.NoError(t, err)

				decryptedRegistrationToken = string(decryptedRegistrationTokenBytes)
			}

			assert.Equal(t, tt.expectedRegistrationToken, decryptedRegistrationToken)
		})
	}
}

func TestRegistrationUseCase_FetchCertificate(t *testing.T) {
	observedZapCore, _ := observer.New(zap.InfoLevel)

	observedLogger := zap.New(observedZapCore)

	appKeyPair, err := ec.GenerateKeyPair(elliptic.P256())
	assert.NoError(t, err)

	appPublicKeyPem, err := appKeyPair.PublicKey.ToPem()
	assert.NoError(t, err)

	organizationRootKeyPair, err := ec.GenerateKeyPair(elliptic.P256())
	assert.NoError(t, err)

	organizationRootPublicKeyPem, err := organizationRootKeyPair.PublicKey.ToPem()
	assert.NoError(t, err)

	sessionAesKey, err := aes.GenerateKey(aes.AES128)
	assert.NoError(t, err)

	type fields struct {
		RegistrationRepository       serviceRepositories.RegistrationRepository
		SessionRepository            sessionRepositories.SessionRepository
		BkConfigRepository           bkConfigRepositories.BkConfigRepository
		CertificateExpirationMinutes int
	}
	type args struct {
		sessionToken      string
		registrationToken string
	}
	type expected struct {
		CertificateType     string
		AppPublicKey        string
		AppManagerPublicKey string
		GivenName           string
		FamilyName          string
		Birthdate           string
		Bsn                 string
		Scope               string
		Iat                 float64
		Nbf                 float64
		Exp                 float64
	}

	tests := []struct {
		name          string
		fields        fields
		args          args
		expectedError error
		expected      expected
	}{
		{
			name: "returns error as session does not exists",
			fields: fields{
				SessionRepository: func() sessionRepositories.SessionRepository {
					controller := gomock.NewController(t)

					sessionRepository := mock.NewMockSessionRepository(controller)

					sessionRepository.
						EXPECT().
						GetSession(gomock.Any()).
						Return(
							nil,
							errors.New("session does not exist"),
						)

					return sessionRepository
				}(),
			},
			expectedError: fmt.Errorf("failed to get session: %v", errors.New("session does not exist")),
		},
		{
			name: "returns session expired error when session is expired",
			fields: fields{
				SessionRepository: func() sessionRepositories.SessionRepository {
					controller := gomock.NewController(t)

					sessionRepository := mock.NewMockSessionRepository(controller)

					dateTime := time.Now().Add(time.Minute * -5)

					sessionRepository.
						EXPECT().
						GetSession(gomock.Any()).
						Return(
							&sessionServiceModel.Session{
								Token:     "token",
								CreatedAt: sessionServiceModel.JSONTime(dateTime),
								ExpiresAt: sessionServiceModel.JSONTime(dateTime.Add(time.Minute * 5)),
							},
							nil,
						)

					return sessionRepository
				}(),
			},
			expectedError: usecases.ErrSessionExpired,
		},
		{
			name: "returns registration expired error when registration token is expired",
			fields: fields{
				SessionRepository: func() sessionRepositories.SessionRepository {
					controller := gomock.NewController(t)

					sessionRepository := mock.NewMockSessionRepository(controller)

					dateTime := time.Now()

					appPublicKeyPem, err := appKeyPair.PublicKey.ToPem()
					assert.NoError(t, err)

					sessionRepository.
						EXPECT().
						GetSession(gomock.Any()).
						Return(
							&sessionServiceModel.Session{
								Token:        "token",
								AppPublicKey: string(appPublicKeyPem),
								CreatedAt:    sessionServiceModel.JSONTime(dateTime),
								ExpiresAt:    sessionServiceModel.JSONTime(dateTime.Add(time.Minute * 5)),
							},
							nil,
						)

					return sessionRepository
				}(),
				RegistrationRepository: func() serviceRepositories.RegistrationRepository {
					controller := gomock.NewController(t)

					registrationRepository := mock.NewMockRegistrationRepository(controller)

					registrationToken := "registrationToken"

					dateTime := time.Now().Add(time.Minute * -5)

					appPublicKeyPem, err := appKeyPair.PublicKey.ToPem()
					assert.NoError(t, err)

					registrationRepository.
						EXPECT().
						Get(gomock.Any()).
						Return(
							&serviceModel.Registration{
								Token:        registrationToken,
								Bsn:          "123456789",
								CreatedAt:    serviceModel.JSONTime(dateTime),
								ExpiresAt:    serviceModel.JSONTime(dateTime.Add(time.Minute * 5)),
								AppPublicKey: string(appPublicKeyPem),
							},
							nil,
						)

					return registrationRepository
				}(),
			},
			expectedError: usecases.ErrRegistrationExpired,
		},
		{
			name: "returns error session app public key and app identity public does not match",
			fields: fields{
				SessionRepository: func() sessionRepositories.SessionRepository {
					controller := gomock.NewController(t)

					sessionRepository := mock.NewMockSessionRepository(controller)

					dateTime := time.Now()

					appPublicKeyPem, err := appKeyPair.PublicKey.ToPem()
					assert.NoError(t, err)

					sessionRepository.
						EXPECT().
						GetSession(gomock.Any()).
						Return(
							&sessionServiceModel.Session{
								Token:        "token",
								AppPublicKey: string(appPublicKeyPem) + "asdf",
								CreatedAt:    sessionServiceModel.JSONTime(dateTime),
								ExpiresAt:    sessionServiceModel.JSONTime(dateTime.Add(time.Minute * 5)),
							},
							nil,
						)

					return sessionRepository
				}(),
				RegistrationRepository: func() serviceRepositories.RegistrationRepository {
					controller := gomock.NewController(t)

					registrationRepository := mock.NewMockRegistrationRepository(controller)

					registrationToken := "registrationToken"

					dateTime := time.Now().Add(time.Minute)

					appPublicKeyPem, err := appKeyPair.PublicKey.ToPem()
					assert.NoError(t, err)

					registrationRepository.
						EXPECT().
						Get(gomock.Any()).
						Return(
							&serviceModel.Registration{
								Token:        registrationToken,
								Bsn:          "123456789",
								CreatedAt:    serviceModel.JSONTime(dateTime),
								ExpiresAt:    serviceModel.JSONTime(dateTime.Add(time.Minute * 5)),
								AppPublicKey: string(appPublicKeyPem),
							},
							nil,
						)

					return registrationRepository
				}(),
			},
			expectedError: fmt.Errorf("session app public key and app identity public does not match"),
		},
		{
			name: "returns encrypted certificate",
			fields: fields{
				SessionRepository: func() sessionRepositories.SessionRepository {
					controller := gomock.NewController(t)

					sessionRepository := mock.NewMockSessionRepository(controller)

					dateTime := time.Now()

					appPublicKeyPem, err := appKeyPair.PublicKey.ToPem()
					assert.NoError(t, err)

					sessionRepository.
						EXPECT().
						GetSession(gomock.Any()).
						Return(
							&sessionServiceModel.Session{
								Token:        "token",
								AppPublicKey: string(appPublicKeyPem),
								AesKey:       base64.ParseFromBytes(sessionAesKey),
								CreatedAt:    sessionServiceModel.JSONTime(dateTime),
								ExpiresAt:    sessionServiceModel.JSONTime(dateTime.Add(time.Minute * 5)),
							},
							nil,
						)

					return sessionRepository
				}(),
				RegistrationRepository: func() serviceRepositories.RegistrationRepository {
					controller := gomock.NewController(t)

					registrationRepository := mock.NewMockRegistrationRepository(controller)

					registrationToken := "registrationToken"

					dateTime := time.Now()

					registrationRepository.
						EXPECT().
						Get(gomock.Any()).
						Return(
							&serviceModel.Registration{
								Token:        registrationToken,
								Bsn:          "814859094",
								CreatedAt:    serviceModel.JSONTime(dateTime),
								ExpiresAt:    serviceModel.JSONTime(dateTime.Add(time.Minute * 5)),
								AppPublicKey: string(appPublicKeyPem),
							},
							nil,
						)

					return registrationRepository
				}(),
				BkConfigRepository: func() bkConfigRepositories.BkConfigRepository {
					controller := gomock.NewController(t)

					bkConfigRepository := mock.NewMockBkConfigRepository(controller)

					organizationRootKeyPairPem, err := organizationRootKeyPair.ToPem()
					assert.NoError(t, err)

					bkConfigRepository.
						EXPECT().
						ListKeyPairs().
						Return(
							&[]bkConfigServiceModel.KeyPair{
								{
									Id:         "1",
									PrivateKey: string(organizationRootKeyPairPem),
									PublicKey:  string(organizationRootPublicKeyPem),
								},
							},
							nil,
						)

					return bkConfigRepository
				}(),
				CertificateExpirationMinutes: 5,
			},
			expected: expected{
				CertificateType:     "AppManagerJWTCertificate",
				AppPublicKey:        string(appPublicKeyPem),
				AppManagerPublicKey: string(organizationRootPublicKeyPem),
				GivenName:           "Piet",
				FamilyName:          "Verstraten",
				Birthdate:           "1995-03-10",
				Bsn:                 "814859094",
				Scope:               "nl.vorijk.oauth_scope.blauwe_knop",
				// Iat:                 "",
				// Nbf:                 "",
				// Exp:                 "",
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			citizenFinancialClaimUseCase := usecases.NewRegistrationUseCase(
				observedLogger,
				tt.fields.RegistrationRepository,
				tt.fields.SessionRepository,
				tt.fields.BkConfigRepository,
				0,
				tt.fields.CertificateExpirationMinutes,
			)

			actualEncryptedCertificate, err := citizenFinancialClaimUseCase.FetchCertificate(
				tt.args.sessionToken,
				tt.args.registrationToken)
			assert.Equal(t, tt.expectedError, err)

			if err == nil {
				certificateBytes, err := actualEncryptedCertificate.ToBytes()
				assert.NoError(t, err)

				decryptedCertificateBytes, err := aes.Decrypt(sessionAesKey, certificateBytes)
				assert.NoError(t, err)

				var certificate model.Certificate
				err = json.Unmarshal(decryptedCertificateBytes, &certificate)
				assert.NoError(t, err)

				token, err := jwt.Parse(string(certificate.Value), func(jwtToken *jwt.Token) (interface{}, error) {
					return organizationRootKeyPair.PublicKey.ToEcdsa(), nil
				})
				assert.NoError(t, err)

				claims, ok := token.Claims.(jwt.MapClaims)
				assert.True(t, ok)

				assert.Equal(t, tt.expected.CertificateType, certificate.Type)
				assert.Equal(t, tt.expected.AppManagerPublicKey, claims["app_manager_public_key"].(string))
				assert.Equal(t, tt.expected.AppPublicKey, claims["app_public_key"].(string))
				assert.Equal(t, tt.expected.Bsn, claims["bsn"].(string))
				assert.Equal(t, tt.expected.FamilyName, claims["family_name"].(string))
				assert.Equal(t, tt.expected.GivenName, claims["given_name"].(string))
				assert.Equal(t, tt.expected.Scope, "nl.vorijk.oauth_scope.blauwe_knop")
				// assert.Equal(t, tt.expected.Iat, claims["iat"].(float64))
				// assert.Equal(t, tt.expected.Nbf, claims["nbf"].(float64))
				// assert.Equal(t, tt.expected.Exp, claims["exp"].(float64))
			}
		})
	}
}
