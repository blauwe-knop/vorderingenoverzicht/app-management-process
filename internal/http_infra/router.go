// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package http_infra

import (
	"context"
	"net/http"

	"github.com/go-chi/cors"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"go.uber.org/zap"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/app-management-process/internal/http_infra/metrics"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"gitlab.com/blauwe-knop/common/health-checker/pkg/healthcheck"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/app-management-process/internal/usecases"
)

type key int

const (
	registrationUseCaseKey key = iota
	loggerKey              key = iota
)

func NewRouter(registrationUseCase *usecases.RegistrationUseCase, logger *zap.Logger) *chi.Mux {
	r := chi.NewRouter()

	cors := cors.New(cors.Options{
		AllowedMethods: []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders: []string{"Authorization"},
	})
	r.Use(cors.Handler)

	metricsMiddleware := metrics.NewMiddleware("app-management-process")
	r.Use(metricsMiddleware)
	r.Handle("/metrics", promhttp.Handler())

	r.Route("/v1", func(r chi.Router) {
		r.Use(middleware.SetHeader("API-Version", "1.0.0"))

		r.Route("/register_app", func(r chi.Router) {
			r.Use(middleware.Logger)
			r.Post("/", func(responseWriter http.ResponseWriter, request *http.Request) {
				ctx := context.WithValue(request.Context(), registrationUseCaseKey, registrationUseCase)
				ctx = context.WithValue(ctx, loggerKey, logger)
				handlerRegisterApp(responseWriter, request.WithContext(ctx))
			})
		})

		r.Route("/link_user_identity", func(r chi.Router) {
			r.Use(middleware.Logger)
			r.Post("/", func(responseWriter http.ResponseWriter, request *http.Request) {
				ctx := context.WithValue(request.Context(), registrationUseCaseKey, registrationUseCase)
				ctx = context.WithValue(ctx, loggerKey, logger)
				handlerLinkUserIdentity(responseWriter, request.WithContext(ctx))
			})
		})

		r.Route("/fetch_certificate", func(r chi.Router) {
			r.Use(middleware.Logger)
			r.Post("/", func(responseWriter http.ResponseWriter, request *http.Request) {
				ctx := context.WithValue(request.Context(), registrationUseCaseKey, registrationUseCase)
				ctx = context.WithValue(ctx, loggerKey, logger)
				handlerFetchCertificate(responseWriter, request.WithContext(ctx))
			})
		})

		r.Route("/unregister_app", func(r chi.Router) {
			r.Use(middleware.Logger)
			r.Post("/", func(responseWriter http.ResponseWriter, request *http.Request) {
				ctx := context.WithValue(request.Context(), registrationUseCaseKey, registrationUseCase)
				ctx = context.WithValue(ctx, loggerKey, logger)
				handlerUnregisterApp(responseWriter, request.WithContext(ctx))
			})
		})

		r.Get("/openapi.json", func(responseWriter http.ResponseWriter, request *http.Request) {
			ctx := context.WithValue(request.Context(), loggerKey, logger)
			handlerJson(responseWriter, request.WithContext(ctx))
		})

		r.Get("/openapi.yaml", func(responseWriter http.ResponseWriter, request *http.Request) {
			ctx := context.WithValue(request.Context(), loggerKey, logger)
			handlerYaml(responseWriter, request.WithContext(ctx))
		})

		healthCheckHandler := healthcheck.NewHandler("app-management-process", registrationUseCase.GetHealthChecks())
		r.Route("/health", func(r chi.Router) {
			r.Get("/", healthCheckHandler.HandleHealth)
			r.Get("/check", healthCheckHandler.HandlerHealthCheck)
		})
	})

	return r
}
