// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package http_infra

import (
	"encoding/json"
	"errors"
	"net/http"

	"go.uber.org/zap"

	"gitlab.com/blauwe-knop/connect/go-connect/encoding/base64"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/app-management-process/internal/usecases"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/app-management-process/pkg/events"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/app-management-process/pkg/model"
)

func handlerRegisterApp(responseWriter http.ResponseWriter, request *http.Request) {
	context := request.Context()
	registrationUseCase, _ := context.Value(registrationUseCaseKey).(*usecases.RegistrationUseCase)
	logger, _ := context.Value(loggerKey).(*zap.Logger)

	event := events.AMP_14
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))

	providedSessionToken := request.Header.Get("Authorization")

	var encryptedAppIdentity base64.Base64String
	err := json.NewDecoder(request.Body).Decode(&encryptedAppIdentity)
	if err != nil {
		event := events.AMP_21
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	registration, err := registrationUseCase.RegisterApp(providedSessionToken, encryptedAppIdentity)
	if errors.Is(err, usecases.ErrSessionExpired) {
		event := events.AMP_39
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "session expired", http.StatusUnauthorized)
		return
	}
	if err != nil {
		event := events.AMP_22
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	responseWriter.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(responseWriter).Encode(registration)
	if err != nil {
		event := events.AMP_23
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}
	event = events.AMP_15
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
}

func handlerLinkUserIdentity(responseWriter http.ResponseWriter, request *http.Request) {
	context := request.Context()
	registrationUseCase, _ := context.Value(registrationUseCaseKey).(*usecases.RegistrationUseCase)
	logger, _ := context.Value(loggerKey).(*zap.Logger)

	event := events.AMP_16
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))

	registrationToken := request.Header.Get("Authorization")

	var userIdentity model.UserIdentity
	err := json.NewDecoder(request.Body).Decode(&userIdentity)
	if err != nil {
		event := events.AMP_21
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	registration, err := registrationUseCase.LinkUserIdentity(registrationToken, userIdentity)

	if err != nil {
		event := events.AMP_24
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	responseWriter.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(responseWriter).Encode(registration)
	if err != nil {
		event := events.AMP_23
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}
	event = events.AMP_17
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
}

func handlerFetchCertificate(responseWriter http.ResponseWriter, request *http.Request) {
	context := request.Context()
	registrationUseCase, _ := context.Value(registrationUseCaseKey).(*usecases.RegistrationUseCase)
	logger, _ := context.Value(loggerKey).(*zap.Logger)

	event := events.AMP_18
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))

	sessionToken := request.Header.Get("Authorization")
	registrationToken := request.URL.Query().Get("registrationToken")

	fetchCertificateResponse, err := registrationUseCase.FetchCertificate(sessionToken, registrationToken)
	if errors.Is(err, usecases.ErrSessionExpired) {
		event := events.AMP_39
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "session expired", http.StatusUnauthorized)
		return
	}
	if err != nil {
		event := events.AMP_25
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	responseWriter.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(responseWriter).Encode(fetchCertificateResponse)
	if err != nil {
		event := events.AMP_23
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}
	event = events.AMP_19
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
}

func handlerUnregisterApp(responseWriter http.ResponseWriter, request *http.Request) {
	context := request.Context()
	registrationUseCase, _ := context.Value(registrationUseCaseKey).(*usecases.RegistrationUseCase)
	logger, _ := context.Value(loggerKey).(*zap.Logger)

	event := events.AMP_20
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))

	sessionToken := request.Header.Get("Authorization")

	var unregisterAppRequest model.UnregisterAppRequest
	err := json.NewDecoder(request.Body).Decode(&unregisterAppRequest)
	if err != nil {
		event := events.AMP_21
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	err = registrationUseCase.UnregisterApp(sessionToken, unregisterAppRequest)
	if errors.Is(err, usecases.ErrSessionExpired) {
		event := events.AMP_39
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "session expired", http.StatusUnauthorized)
		return
	}
	if err != nil {
		event := events.AMP_26
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}
}
