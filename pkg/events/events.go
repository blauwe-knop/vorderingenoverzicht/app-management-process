package events

var (
	AMP_1 = NewEvent(
		"amp_1",
		"started listening",
		Low,
	)
	AMP_2 = NewEvent(
		"amp_2",
		"server closed",
		High,
	)
	AMP_3 = NewEvent(
		"amp_3",
		"received request for openapi spec as JSON",
		Low,
	)
	AMP_4 = NewEvent(
		"amp_4",
		"sent response with openapi spec as JSON",
		Low,
	)
	AMP_5 = NewEvent(
		"amp_5",
		"received request for openapi spec as YAML",
		Low,
	)
	AMP_6 = NewEvent(
		"amp_6",
		"sent response with openapi spec as YAML",
		Low,
	)
	AMP_8 = NewEvent(
		"amp_8",
		"failed to read openapi.json file",
		High,
	)
	AMP_9 = NewEvent(
		"amp_9",
		"failed to write fileBytes to response",
		High,
	)
	AMP_10 = NewEvent(
		"amp_10",
		"failed to read openapi.yaml file",
		High,
	)
	AMP_14 = NewEvent(
		"amp_14",
		"received request to register app",
		Low,
	)
	AMP_15 = NewEvent(
		"amp_15",
		"sent response with new app registration",
		Low,
	)
	AMP_16 = NewEvent(
		"amp_16",
		"received request to link user identity",
		Low,
	)
	AMP_17 = NewEvent(
		"amp_17",
		"sent response of registration with linked user identity",
		Low,
	)
	AMP_18 = NewEvent(
		"amp_18",
		"received request to fetch certificate",
		Low,
	)
	AMP_19 = NewEvent(
		"amp_19",
		"sent response with certificate",
		Low,
	)
	AMP_20 = NewEvent(
		"amp_20",
		"received request to unregister app",
		Low,
	)
	AMP_21 = NewEvent(
		"amp_21",
		"failed to decode request payload",
		High,
	)
	AMP_22 = NewEvent(
		"amp_22",
		"failed to register app",
		High,
	)
	AMP_23 = NewEvent(
		"amp_23",
		"failed to encode response payload",
		High,
	)
	AMP_24 = NewEvent(
		"amp_24",
		"failed to link user identity",
		High,
	)
	AMP_25 = NewEvent(
		"amp_25",
		"failed to fetch certificate",
		High,
	)
	AMP_26 = NewEvent(
		"amp_26",
		"failed to unregister app",
		High,
	)
	AMP_27 = NewEvent(
		"amp_27",
		"failed to get session",
		High,
	)
	AMP_28 = NewEvent(
		"amp_28",
		"failed to get private key",
		High,
	)
	AMP_29 = NewEvent(
		"amp_29",
		"failed to decode private key from pem",
		High,
	)
	AMP_30 = NewEvent(
		"amp_30",
		"failed to decode ciphertext from request",
		High,
	)
	AMP_31 = NewEvent(
		"amp_31",
		"failed to decrypt envelope",
		High,
	)
	AMP_32 = NewEvent(
		"amp_32",
		"failed to unmarshal appIdentity",
		High,
	)
	AMP_33 = NewEvent(
		"amp_33",
		"session app public key and app identity public does not match",
		High,
	)
	AMP_34 = NewEvent(
		"amp_34",
		"failed to parse app public key to pem",
		High,
	)
	AMP_35 = NewEvent(
		"amp_35",
		"failed to create registration",
		High,
	)
	AMP_36 = NewEvent(
		"amp_36",
		"failed to get registration session",
		High,
	)
	AMP_37 = NewEvent(
		"amp_37",
		"registration expired",
		High,
	)
	AMP_38 = NewEvent(
		"amp_38",
		"failed to update registration session",
		High,
	)
	AMP_39 = NewEvent(
		"amp_39",
		"session expired",
		High,
	)
	AMP_40 = NewEvent(
		"amp_40",
		"failed to get registration",
		High,
	)
	AMP_41 = NewEvent(
		"amp_41",
		"failed retrieve key pair",
		High,
	)
	// Deprecated: AMP_42 is deprecated.
	AMP_42 = NewEvent(
		"amp_42",
		"failed to decode key pair public key from pem",
		High,
	)
	AMP_43 = NewEvent(
		"amp_43",
		"failed to sign access token",
		High,
	)
	AMP_44 = NewEvent(
		"amp_44",
		"failed to marshal certificate",
		High,
	)
	AMP_45 = NewEvent(
		"amp_45",
		"failed to encrypt certificate",
		High,
	)
	AMP_46 = NewEvent(
		"amp_46",
		"failed to encode encrypted certificate",
		High,
	)
	// Deprecated: AMP_47 is deprecated.
	AMP_47 = NewEvent(
		"amp_47",
		"failed to verify registration signature",
		High,
	)
	AMP_48 = NewEvent(
		"amp_48",
		"invalid registration signature",
		High,
	)
	AMP_49 = NewEvent(
		"amp_49",
		"failed to delete registration",
		High,
	)
	AMP_50 = NewEvent(
		"amp_50",
		"failed to list key pairs",
		High,
	)
	AMP_51 = NewEvent(
		"amp_51",
		"key pair does not exist",
		High,
	)
	AMP_52 = NewEvent(
		"amp_52",
		"failed to parse registrationExpirationDays to int",
		VeryHigh,
	)
	AMP_53 = NewEvent(
		"amp_53",
		"failed to parse certificateExpirationMinutes to int",
		VeryHigh,
	)
	AMP_54 = NewEvent(
		"amp_54",
		"app public key invalid",
		VeryHigh,
	)
	AMP_55 = NewEvent(
		"amp_55",
		"failed to decode signature",
		VeryHigh,
	)
	AMP_56 = NewEvent(
		"amp_56",
		"failed to decode session aes key",
		VeryHigh,
	)
	AMP_57 = NewEvent(
		"amp_56",
		"session aes key invalid",
		VeryHigh,
	)
	AMP_58 = NewEvent(
		"amp_58",
		"failed to encrypt registration token",
		High,
	)
)
