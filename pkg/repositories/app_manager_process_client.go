// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package repositories

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"time"

	"gitlab.com/blauwe-knop/common/health-checker/pkg/healthcheck"
	"gitlab.com/blauwe-knop/connect/go-connect/encoding/base64"
	serviceModel "gitlab.com/blauwe-knop/vorderingenoverzicht/app-management-service/pkg/model"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/app-management-process/pkg/model"
)

type AppManagementProcessClient struct {
	baseURL string
}

func NewAppManagementProcessClient(baseURL string) *AppManagementProcessClient {
	return &AppManagementProcessClient{
		baseURL: baseURL,
	}
}

func (s *AppManagementProcessClient) RegisterApp(sessionToken string, encryptedAppIdentity base64.Base64String) (base64.Base64String, error) {
	url := fmt.Sprintf("%s/register_app", s.baseURL)

	requestBodyAsJson, err := json.Marshal(encryptedAppIdentity)
	if err != nil {
		return "", fmt.Errorf("failed to marshall request body: %v", err)
	}

	request, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(requestBodyAsJson))
	if err != nil {
		return "", fmt.Errorf("failed to post RegisterApp request: %v", err)
	}

	request.Header.Set("Authorization", sessionToken)
	request.Header.Set("Content-Type", "application/json")

	client := &http.Client{Timeout: 20 * time.Second}
	resp, err := client.Do(request)
	if err != nil {
		return "", fmt.Errorf("failed to post RegisterApp: %v", err)
	}

	if resp.StatusCode != http.StatusOK {
		return "", fmt.Errorf("unexpected status while retrieving RegisterApp: %d", resp.StatusCode)
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return "", fmt.Errorf("failed to read body: %v", err)
	}

	var registrationToken base64.Base64String
	err = json.Unmarshal(body, &registrationToken)
	if err != nil {
		return "", fmt.Errorf("failed to decode json, session: %v", err)
	}

	return registrationToken, nil

}

func (s *AppManagementProcessClient) LinkUserIdentity(registrationToken string, userIdentity model.UserIdentity) (*serviceModel.Registration, error) {
	url := fmt.Sprintf("%s/link_user_identity", s.baseURL)

	requestBodyAsJson, err := json.Marshal(userIdentity)
	if err != nil {
		return nil, fmt.Errorf("failed to marshall request body: %v", err)
	}

	request, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(requestBodyAsJson))
	if err != nil {
		return nil, fmt.Errorf("failed to post LinkUserIdentity request: %v", err)
	}

	request.Header.Set("Authorization", registrationToken)
	request.Header.Set("Content-Type", "application/json")

	client := &http.Client{Timeout: 20 * time.Second}
	resp, err := client.Do(request)
	if err != nil {
		return nil, fmt.Errorf("failed to post LinkUserIdentity: %v", err)
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected status while retrieving LinkUserIdentity: %d", resp.StatusCode)
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("failed to read body: %v", err)
	}

	registration := serviceModel.Registration{}
	err = json.Unmarshal(body, &registration)
	if err != nil {
		return nil, fmt.Errorf("failed to decode json, registration with token: %s: %v", registrationToken, err)
	}

	return &registration, nil
}

func (s *AppManagementProcessClient) FetchCertificate(sessionToken string, registrationToken string) (base64.Base64String, error) {
	url := fmt.Sprintf("%s/fetch_certificate?registrationToken=%s", s.baseURL, registrationToken)

	request, err := http.NewRequest(http.MethodPost, url, nil)
	if err != nil {
		return "", fmt.Errorf("failed to post FetchCertificate request: %v", err)
	}

	request.Header.Set("Authorization", sessionToken)
	request.Header.Set("Content-Type", "application/json")

	client := &http.Client{Timeout: 20 * time.Second}
	resp, err := client.Do(request)
	if err != nil {
		return "", fmt.Errorf("failed to post FetchCertificate: %v", err)
	}

	if resp.StatusCode != http.StatusOK {
		return "", fmt.Errorf("unexpected status while retrieving FetchCertificate: %d", resp.StatusCode)
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return "", fmt.Errorf("failed to read body: %v", err)
	}

	var encodedCertificate base64.Base64String
	err = json.Unmarshal(body, &encodedCertificate)
	if err != nil {
		return "", fmt.Errorf("failed to decode json, fetchCertificateResponse with token: %s: %v", sessionToken, err)
	}

	return encodedCertificate, nil
}

func (s *AppManagementProcessClient) UnregisterApp(sessionToken string, unregisterAppRequest model.UnregisterAppRequest) error {
	url := fmt.Sprintf("%s/unregister_app", s.baseURL)

	requestBodyAsJson, err := json.Marshal(unregisterAppRequest)
	if err != nil {
		return fmt.Errorf("failed to marshall request body: %v", err)
	}

	request, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(requestBodyAsJson))
	if err != nil {
		return fmt.Errorf("failed to post UnregisterApp request: %v", err)
	}

	request.Header.Set("Authorization", sessionToken)
	request.Header.Set("Content-Type", "application/json")

	client := &http.Client{Timeout: 20 * time.Second}
	resp, err := client.Do(request)
	if err != nil {
		return fmt.Errorf("failed to post UnregisterApp: %v", err)
	}

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("unexpected status while retrieving UnregisterApp: %d", resp.StatusCode)
	}

	return nil
}

func (s *AppManagementProcessClient) GetHealth() error {
	url := fmt.Sprintf("%s/health", s.baseURL)
	resp, err := http.Get(url)
	if err != nil {
		return fmt.Errorf("failed to fetch app management process health: %v", err)
	}

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("failed to fetch app management process health with status: %d", resp.StatusCode)
	}

	return nil
}

func (s *AppManagementProcessClient) GetHealthCheck() healthcheck.Result {
	name := "app-management-process"
	url := fmt.Sprintf("%s/health/check", s.baseURL)
	timeout := 10 * time.Second
	start := time.Now()

	client := http.Client{
		Timeout: timeout,
	}

	request, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		log.Printf("failed to create health check request: %v", err)
		return healthcheck.Result{
			Name:         name,
			Status:       healthcheck.StatusError,
			ResponseTime: time.Since(start).Seconds(),
			HealthChecks: []healthcheck.Result{},
		}
	}

	request.Header.Set("Content-Type", "application/json")

	response, err := client.Do(request)
	if err != nil {
		log.Printf("failed to get health check request: %v", err)
		return healthcheck.Result{
			Name:         name,
			Status:       healthcheck.StatusError,
			ResponseTime: time.Since(start).Seconds(),
			HealthChecks: []healthcheck.Result{},
		}
	}

	body, err := io.ReadAll(response.Body)

	if response.StatusCode != http.StatusOK && response.StatusCode != http.StatusServiceUnavailable {
		if err != nil {
			log.Printf("failed to parse body health check request: %s", err.Error())
		} else {
			log.Printf("failed to parse body health check request: %d - %s", response.StatusCode, string(body))
		}

		return healthcheck.Result{
			Name:         name,
			Status:       healthcheck.StatusError,
			ResponseTime: time.Since(start).Seconds(),
			HealthChecks: []healthcheck.Result{},
		}
	}

	var healthCheckResponse healthcheck.Result

	err = json.Unmarshal(body, &healthCheckResponse)
	if err != nil {
		log.Printf("Unmarshal error: %v", err)
		healthCheckResponse.Status = healthcheck.StatusError
	}

	return healthCheckResponse
}
