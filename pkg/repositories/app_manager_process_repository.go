// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package repositories

import (
	"gitlab.com/blauwe-knop/common/health-checker/pkg/healthcheck"
	"gitlab.com/blauwe-knop/connect/go-connect/encoding/base64"

	serviceModel "gitlab.com/blauwe-knop/vorderingenoverzicht/app-management-service/pkg/model"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/app-management-process/pkg/model"
)

type AppManagementProcessRepository interface {
	RegisterApp(sessionToken string, encryptedAppIdentity base64.Base64String) (base64.Base64String, error)
	LinkUserIdentity(registrationToken string, userIdentity model.UserIdentity) (*serviceModel.Registration, error)
	FetchCertificate(sessionToken string, registrationToken string) (base64.Base64String, error)
	UnregisterApp(sessionToken string, unregisterAppRequest model.UnregisterAppRequest) error
	healthcheck.Checker
}
