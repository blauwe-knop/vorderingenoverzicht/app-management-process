// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package model

type AppIdentity struct {
	AppPublicKey string `json:"appPublicKey"`
	ClientId     string `json:"clientId"`
}
