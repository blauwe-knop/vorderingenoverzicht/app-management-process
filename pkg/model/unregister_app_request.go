// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package model

type UnregisterAppRequest struct {
	RegistrationToken string `json:"registrationToken"`
	Signature         string `json:"signature"`
}
