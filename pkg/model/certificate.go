// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package model

type Certificate struct {
	Type  string `json:"type"`
	Value []byte `json:"value"`
}
