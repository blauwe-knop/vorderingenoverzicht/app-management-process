#!/bin/bash

mockgen -destination=./../test/mock/registration_repository.go -package=mock gitlab.com/blauwe-knop/vorderingenoverzicht/app-management-service/pkg/repositories RegistrationRepository
mockgen -destination=./../test/mock/session_repository.go -package=mock gitlab.com/blauwe-knop/vorderingenoverzicht/session-service/pkg/repositories SessionRepository
mockgen -destination=./../test/mock/bk_config_repository.go -package=mock gitlab.com/blauwe-knop/vorderingenoverzicht/bk-config-service/pkg/repositories BkConfigRepository
