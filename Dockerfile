FROM golang:1.23.1-alpine3.20 AS build

RUN apk add --update --no-cache git

COPY ./api /go/src/app-management-process/api
COPY ./cmd /go/src/app-management-process/cmd
COPY ./internal /go/src/app-management-process/internal
COPY ./pkg /go/src/app-management-process/pkg
COPY ./go.mod /go/src/app-management-process/
COPY ./go.sum /go/src/app-management-process/
WORKDIR /go/src/app-management-process
RUN go mod download \
 && go build -o dist/bin/app-management-process ./cmd/app-management-process

FROM alpine:3.20

ARG USER_ID=10001
ARG GROUP_ID=10001

COPY --from=build /go/src/app-management-process/dist/bin/app-management-process /usr/local/bin/app-management-process
COPY --from=build /go/src/app-management-process/api/openapi.json /api/openapi.json
COPY --from=build /go/src/app-management-process/api/openapi.yaml /api/openapi.yaml

# Add non-priveleged user. Disabled for openshift
RUN addgroup -g "$GROUP_ID" appgroup \
 && adduser -D -u "$USER_ID" -G appgroup appuser
USER appuser
HEALTHCHECK none
CMD ["/usr/local/bin/app-management-process"]
