// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package main

import (
	"errors"
	"fmt"
	"log"
	"net/http"
	"strconv"

	"go.uber.org/zap"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/app-management-service/pkg/repositories"
	bkConfigRepositories "gitlab.com/blauwe-knop/vorderingenoverzicht/bk-config-service/pkg/repositories"
	sessionRepositories "gitlab.com/blauwe-knop/vorderingenoverzicht/session-service/pkg/repositories"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/app-management-process/internal/http_infra"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/app-management-process/internal/usecases"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/app-management-process/pkg/events"
)

type options struct {
	ListenAddress                string `long:"listen-address" env:"LISTEN_ADDRESS" default:"0.0.0.0:80" description:"Address for the app-management-process api to listen on. Read https://golang.org/pkg/net/#Dial for possible tcp address specs."`
	AppManagementServiceAddress  string `long:"app-management-service-address" env:"APP_MANAGEMENT_SERVICE_ADDRESS" default:"http://localhost:80" description:"Registration service address."`
	AppManagementServiceAPIKey   string `long:"app-management-service-api-key" env:"APP_MANAGEMENT_SERVICE_API_KEY" default:"" description:"API key to use when calling the Registration service."`
	SessionServiceAddress        string `long:"session-service-address" env:"SESSION_SERVICE_ADDRESS" default:"http://localhost:80" description:"Session service address."`
	SessionServiceAPIKey         string `long:"session-service-api-key" env:"SESSION_SERVICE_API_KEY" default:"" description:"API key to use when calling the session service."`
	BkConfigServiceAddress       string `long:"bk-config-service-address" env:"BK_CONFIG_SERVICE_ADDRESS" default:"http://localhost:80" description:"Bk Config service address."`
	BkConfigServiceAPIKey        string `long:"bk-config-service-api-key" env:"BK_CONFIG_SERVICE_API_KEY" default:"" description:"API key to use when calling the bk-config service."`
	RegistrationExpirationHours  string `long:"registration-expiration-hours" env:"REGISTRATION_EXPIRATION_HOURS" default:"1" description:"Max length of a registration in hours."`
	CertificateExpirationMinutes string `long:"certificate-expiration-minutes" env:"CERTIFICATE_EXPIRATION_MINUTES" default:"5" description:"Max length of a certificate in minutes."`

	LogOptions
}

func main() {
	cliOptions, err := parseOptions()
	if err != nil {
		log.Fatalf("failed to parse options: %v", err)
	}

	logger, err := newLogger(cliOptions.LogOptions)
	if err != nil {
		log.Fatalf("failed to create logger: %v", err)
	}

	registrationRepository := repositories.NewRegistrationClient(cliOptions.AppManagementServiceAddress, cliOptions.AppManagementServiceAPIKey)
	sessionRepository := sessionRepositories.NewSessionClient(cliOptions.SessionServiceAddress, cliOptions.SessionServiceAPIKey)
	bkConfigRepository := bkConfigRepositories.NewBkConfigClient(cliOptions.BkConfigServiceAddress, cliOptions.BkConfigServiceAPIKey)

	registrationExpirationHours, err := strconv.Atoi(cliOptions.RegistrationExpirationHours)
	if err != nil {
		event := events.AMP_52
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
	}

	certificateExpirationMinutes, err := strconv.Atoi(cliOptions.CertificateExpirationMinutes)
	if err != nil {
		event := events.AMP_53
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
	}

	registrationUseCase := usecases.NewRegistrationUseCase(
		logger,
		registrationRepository,
		sessionRepository,
		bkConfigRepository,
		registrationExpirationHours,
		certificateExpirationMinutes,
	)

	router := http_infra.NewRouter(registrationUseCase, logger)

	event := events.AMP_1
	logger.Log(event.GetLogLevel(), event.Message, zap.String("listenAddress", cliOptions.ListenAddress), zap.Reflect("event", event))
	err = http.ListenAndServe(cliOptions.ListenAddress, router)
	if err != nil {
		event = events.AMP_2
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		if !errors.Is(err, http.ErrServerClosed) {
			panic(err)
		}
	}
}

func newLogger(logOptions LogOptions) (*zap.Logger, error) {
	config := logOptions.ZapConfig()
	logger, err := config.Build()
	if err != nil {
		return nil, fmt.Errorf("failed to create new zap logger: %v", err)
	}

	return logger, nil
}
